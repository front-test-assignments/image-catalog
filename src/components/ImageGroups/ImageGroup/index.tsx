import { Card } from 'antd';

const gridStyle: React.CSSProperties = {
    width: "auto",
    textAlign: "center",
};

export interface Image {
    tag: string, 
    url: string
};


interface ImageGroupProps {
    title: string, 
    images: Image[], 
    clickHandler: (event: any) => void
}

const ImageGroup = ({ title, images, clickHandler }: ImageGroupProps) => {
    const renderImages = (imageList: Image[]) => imageList.map((image) => renderGrid(image))
    const renderGrid = (image: Image) => {
        return (
            <Card.Grid style={gridStyle}>
                <img src={image.url} alt={image.tag} onClick={clickHandler}/>
            </Card.Grid>
        )
    }
    
    return (
        <Card title={title}>
            {renderImages(images)}
        </Card>  
    )
}

export default ImageGroup