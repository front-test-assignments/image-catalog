import ImageGroup from './ImageGroup/index'
import { uniq } from 'lodash'
import { Image } from  './ImageGroup/index'

interface ImageGroupsProps {
    imageList: Image[][],
    showByCategory: boolean,
    setInputValue: (event: any) => void
}

const ImageGroups = ({ imageList, showByCategory, setInputValue }: ImageGroupsProps): JSX.Element => {
    const flatImageList = imageList.flat();

    const clickHandler = ({ target: { alt: tag }}: {target: {alt: string}}) => setInputValue(tag)

    const groupByTag = (list: Image[]) => {
        const tags = uniq(list.map((image) => image.tag))
        return tags.map((tag) => list.filter((image) => image.tag === tag))
    }

    const groupList = (): JSX.Element[] => {
        const groupedImageList = groupByTag(flatImageList)
        return groupedImageList.map((list) => <ImageGroup title={list[0].tag} images={list} clickHandler={clickHandler}/>)
    }

    const ungroupedList = (): JSX.Element[] => {    
        return imageList.map((imagesGroup) => {
            if (imagesGroup.length > 1) {
                return <ImageGroup title={'Collection'} images={imagesGroup} clickHandler={clickHandler}/>
            }
            return <ImageGroup title={imagesGroup[0].tag} images={imagesGroup} clickHandler={clickHandler}/>
        })
    }

    return (
        <>
            {showByCategory ? groupList() : ungroupedList()}
        </>
    )

}

export default ImageGroups