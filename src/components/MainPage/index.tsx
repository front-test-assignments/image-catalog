import { Form, Input, Button } from 'antd';
import axios from "axios";
import { useState } from 'react';
import ImageGroups from '../ImageGroups';
import { Config } from '../../Config';
import { Image } from  '../ImageGroups/ImageGroup/index'

const layout = {
    labelCol: {
        span: 13,
    },
    wrapperCol: {
        offset: 9,
        span: 6,
    },
};
  
const tailLayout = {
    wrapperCol: {
        offset: 10,
        span: 16,
    },
};

const MainPage = () => {
    const [ imageList, setImageList ] = useState<Image[][]>([]);
    const [ form ] = Form.useForm();
    const { apiKey } = Config;
    const [ showByCategory, setShowByCategory ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);

    const getPictures = (tags: string[]) => {
        const images =  tags.map((tag) => {
            const image =  axios
                .get(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}&tag=${tag}`)
                .then(({ data: responseData }): Image => {
                    const {data: {image_url: imageUrl}} = responseData;
                    return { url: imageUrl, tag }
                })    
            return image
        })
        return images
    }

    const isValid = (name: string): boolean => {
        const regex = /^[A-Za-z,]+$/;
        return regex.test(name)
    }

    const getRandomTag = (): string => {
        const randomTags = [
            'pusheen',
            'pikachu',
            'mascot',
            'goodmorning',
            'johnwick'
        ];
        const randomIndex = Math.floor(Math.random() * Math.floor(randomTags.length))
        return randomTags[randomIndex];
    };

    const delayStaff = (): NodeJS.Timeout => setInterval(() => formSubmitHandler({ name: getRandomTag()}), 5000);

    const formSubmitHandler = (values: { name: string }) => {
        setIsLoading(true)
        if (values.name === 'delay') {
            return delayStaff()
        }
        const tags = values.name.split(',').filter(({length}) => length > 0)
        Promise
            .allSettled(getPictures(tags))
            .then((results) => {
                const fulfilledPromises = results.filter((x): x is PromiseFulfilledResult<Image> => x.status === "fulfilled")
                const rejectedPromises = results.filter(({status}) => status === 'rejected')
                if (rejectedPromises.length > 0) {
                    alert(`Произошло ${rejectedPromises.length} ошибок в запросах!`)
                }
                const images = fulfilledPromises.map(({value: image}) => image)
                setImageList((list) => [...list, images])
            })
            .then(() => setIsLoading(false))
    };

    
    const clearButtonClickHandler = () => {
        setImageList([]);
        form.resetFields();
    };

    const setInputValue = (tag: string) => {
        form.setFieldsValue({
            name: tag
        })
    };

    const groupButtonText = () => showByCategory ? 'Ungroup' : 'Group';

    const downloadButtonText = () => isLoading ? 'Louding' : 'Download';

    const changeGroupButtonText = () => setShowByCategory(!showByCategory);

    const handleTagQueryKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        const regex = /[A-Za-z,]/;
        if (!regex.test(event.key)) {
            event.nativeEvent.preventDefault();
        }
    }

    return (
        <>
            <Form
                {...layout}
                form={form}
                name="basic"
                onFinish={formSubmitHandler}
            >
                <Form.Item
                    name="name"
                    dependencies={['Tags']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Tag is required',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (isValid(value)) {
                                    return Promise.resolve();
                                }
                                return Promise.reject('Tags shuld be normal!')
                            },
                        }),
                    ]}
                >
                    <Input 
                        placeholder='Cat,dog,unicorn' 
                        onKeyPress={ handleTagQueryKeyPress }
                    />
                </Form.Item>
                <Form.Item {...tailLayout} >
                    <Button type="primary" htmlType="submit" disabled={isLoading}>{downloadButtonText()}</Button>
                    <Button onClick={clearButtonClickHandler} danger>Clear</Button>
                    <Button type="default" onClick={changeGroupButtonText}>{groupButtonText()}</Button>
                </Form.Item>
            </Form>
            <ImageGroups imageList={imageList} showByCategory={showByCategory} setInputValue={setInputValue}/>
        </>  
    )
}

export default MainPage;